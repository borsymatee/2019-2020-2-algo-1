# Közérdekű közlemények

## Alapadatok

* Az itt található anyagok az Algoritmusok és Adatszerkezetek 1. című tárgy 2019/20. tavaszi félévének 7. csoportjának szólnak
* Órák: keddenként, 16.30 - 18.00, D 0-411

## Elérhetőség

* Email: szitab@ik.elte.hu
* Körüzeneteket neptunból fogok küldeni
    * Legyen megadva éles email-cím, hogy egyből értesülj mindenről
* Beadandó-beadások, személyre szabott üzenetek, eredmények: https://canvas.elte.hu (belépés neptunnal)
    * Itt is legyen beállítva az emailes értesítés
* Közös információ, tananyag, beadandó-feladatkiírások: https://gitlab.com/elte-ik-algo/2019-2020-2-algo-1/
* Külön, automatikus konzultációs időpont nincs (mint ahogy szobám sincs), kérdés esetén emailben elérhető vagyok és ha online nem tudjuk megoldani, lehet kérni személyes konzultációt egyedi időpontra
* Órákon, zh-kon az algoritmusokat struktogram formájában adom meg és lehetőleg úgy is kérem vissza (vizsgán mindenképp). Online anyagokban pszeudokódként adom meg és online beadott szorgalmi feladat megoldásoknál el is fogadom a pszeudokódos megoldásokat

## Követelmények

### Tematika

* A gyakorlatokon előreláthatólag ebben a sorrendben és kb. ilyen arányokban vesszük az alábbi témákat:
    * Bevezetés, fogalmak, példák (2)
    * Műveletigény (2)
    * Láncolt listák (3)
    * Vermek és sorok (3)
    * Bináris fák (2)
    * Bináris keresőfák (1)
    * Kupacok, prioritásos sorok (1)
    * Lineáris rendezések (2)
    * Hashelés (2)
* Mint látható, ha a félévet 13 órásnak vesszük, kényelmes ütemezéssel maximum a bináris keresőfákig fogunk tudni eljutni
* Bizonyos témák csak gyakorlaton fognak szerepelni
    * Ilyen például a maximumkiválasztó rendezés - *ez kell vizsgára*
    * És ilyen a lengyelforma témaköre - ha minden igaz, ez csak zh-ra kell, vizsgára nem
    * Ha megtudok ennél többet, módosítom, bővítem ezt a listát

### Órák

* Jelenlét nem kötelező
* Az első hét kivételével mégis számon tartom a bejárási statisztikákat, s akik kitartóbban látogatják az órát, félév végén esetleges két jegy közötti állásnál, jobb elbírálásban lehet részük (az mindegy, hogy kettesért vagy négyesért küzd az illető)
* Amennyiben valamelyik óra a csoport tagjainak zömét érintő (évfolyamszintű) egyéb megpróbáltatással (analízis zh) ütközik, az adott órát elhalasztjuk. Ezt kérem, előre jelezzétek! Az esetleges pótórák idejéről szavazással döntünk
* Első órai többségi szavazás alapján végül abban állapodtunk meg, hogy minden olyan anyagot, amit úgy érzek, hogy a kurzus elsajátításához szükséges, alaposan leadok és ide fel is töltök. Ezzel azt kockáztatjuk, hogy nem érünk az anyag végére. Ha ez így történik, a bele nem férő anyagrészt zh-n nem fogom visszakérni, ám szorgalmi feladatokat írhatok ki hozzá, és mivel a vizsgán előfordulhatnak, segédanyagot is fogok feltölteni ezekhez
* Elképzelhető még több segédanyag felöltése is, ezeket érdemes átolvasni, felhasználni, de a kurzus elvileg nélkülük is teljesíthető. Új ismereteket nem fognak hordozni, csak a tanultakat ismételni, elmélyíteni

### Szorgalmi feladatok

* A félév során összesen 8 alkalommal (első óra és zh-k alkalmával nem) egy rövid "kvíz", amire az alábbi pontokat lehet kapni:
    * 0 pont - ha nincs beadva (~hiányzás)
    * 2 pont - ha be van adva és hibátlan
    * 2 pont - ha nincs a csoportban legalább 5 db hibátlan, s a kvíz az összes csoporttagot számolva a helyesség és gyorsaság szempontjából a legjobb 5 megoldás között van
    * 1 pont - különben
* Bizonyos órai kérdésekre a leggyorsabban jó választ adó (szóban vagy táblánál) kaphat 1 pontot
    * Ezt a lehetőséget a konkrét feladat ismertetése előtt bejelentem
    * Legalább 5 ilyen alkalom várható a félévben
* Órákon változó mennyiségben feladok egy-egy "papíros" szorgalmit 1-1 pontért
    * Legalább 15 db várható a félévben
    * Ezek pontos szövegezése itt, a "00_Feladatok" mappa alatt lesz majd olvasható, beadni pedig a kurzushoz tartozó canvas-felületen kell, akár mint üzenet, akár mint csatolt fájl
    * Elképzelhető, hogy 0 pontos értékelés esetén egy új, szűkebb határidőt tartva az általam megállapított hibákat, hiányosságokat javítva, pótolva a pont megszerezhető - erre vonatkozó jelzést lásd a canvasban az értékelésnél
    * A határidő a canvason megadottak szerint alakul, ami várhatóan és általában a feltöltés + 2 hét
* A félév folyamán az előforduló témák nyomán két alkalommal is kódolós szorgalmi feladatok kerülnek kiírásra
    * Az első alkalommal kb. a félév első felében vett anyagrész alapján írom ki a feladatokat, a második alkalommal a félév második fele alapján
    * Mindkét alkalommal 5-5 feladatból lehet szabadon választani egyet-egyet
    * A kiírás és a beadás hasonlóan alakul, mint az előző pontban, de a határidő valamivel nagyvonalúbb (kb. a vizsgaidőszak eleje) lesz
    * A határidőre, jó minőségben beadott megoldás értéke 5 pont
    * A gyanúsan hasonló megoldások 0 pontot érnek (esetleges eltérés ettől utólag kialakulhat, ha egyértelműen kiderült, hogy ki a forrás és ki a másoló)
    * A gyengébbnek értékelt beadandókat várhatóan még lehet javítani a visszajelzéseim alapján
    * A pontozás során általános szoftvertechnológiai szempontok és az algoritmus helyessége (és a tanultaknak megfeleltsége) fog számítani (részletesebb követelmények a feladatkiírásnál)
    * Dokumentációt nem kell írni (amíg minden egyértelmű), a nyelv tetszőleges

### Zárthelyik

* A félév során 4 db témazáró zárthelyi lesz, mind 20 pontos
* Az óra helyszínén és idejében írjuk ezeket, hosszuk változó, kb. 15-20 percesek lesznek - előttük/utánuk az óra folytatódik további anyagrészekkel
* Várhatóan 2 feladat lesz ezeken, egy lejátszós fókuszú és egy algoritmusírós
* Szerepelhet tanult algoritmust megadó, azt elemző, azt lejátszó feladat vagy olyan kreatívabb feladat, amit bár órán egy az egyben nem vettünk, de a tanultak alapján megoldható
* Egy feladatnak több része is lehet (lejátszás, elméleti kérdés, bizonyítás, vagy legalábbis indoklás, struktogram...)
* Zh hetében nincs kvíz
* A zh jelenléti ívként is szolgál, automatikusan (a 20 ponton túl) jár egy pont a beadott zh-ért
* A zh-k idejét legalább egy héttel az előtt bejelentem, írásban is
* A zh-k előtti héten tartok célzott konzultációt, melyekre az adott zh-ra gyakorló feladatokkal készülök
    * A konzultáció idejéről szavazással döntünk
* "Tételsort" és megoldott minta zh-t elérhetővé teszek, de a zh feladattípusok jellegéből adódóan a valós zh a mintától igencsak eltérhet
* A zh-k tematikájával és időzítésével a fentebb megadott féléves tematikát igyekszem követni, ettől lehet persze eltérés, amit jelezni fogok
* Alapvetően a 4 zh-t így szeretném beosztani - a zh-k darabszáma mindenképpen 4 lesz, a lefedett anyagrészek változhatnak:
    * 1. zh: Bevezető példák, műveletigény
    * 2. zh: Láncolt listák
    * 3. zh: Vermek és sorok
    * 4. zh: Bináris fák és bináris keresőfák
* A zh-ra készülésnek mindenképp legyen része az előadás-jegyzet átolvasása is; bár direkt csak előadás-anyagot nem fogok kérdezni, de a tanuláshoz jól jöhet
* Minimális elérendő pontszám nincs, ha anélkül is összejön a szükséges összpontszám, még megírni se kell
* Az összes zh-t külön-külön egyszer lehet javítani (technikailag akár egy, akár több időpontra bontva ezeket). Rontás esetén az eredeti pontszámot veszem figyelembe
* Ha a szükséges pontszám így se jön össze, még egyszer meg lehet írni a zh-kat, de ekkor kötelezően a négyet egyben 80 pontért (~gyakorlati utóvizsga) - rontani itt se lehet
* A pót- és javítózh-k, valamint gyakuv időpontjait csoportos vagy egyéni megbeszélés alapján a vizsgaidőszak elejére tervezem

### Egyéb

* A részvétel a 2020. tavaszi 5vös5km futóversenyen (bemutatott rajtszámmal, vagy bármi más bizonyítékkal) 5 pontot ér
* A honlapon található súlyos helyesírási, vagy akármilyen tartalmi hiba időrendben első megtalálójának (emailben vagy itt Merge Requestként lehet jelezni) 1 pont jár
* Ha felhívod a figyelmem, hogy szintaktikailag vagy tartalmilag az előadással ellentmondó módon tanítok valamit, szintén jár 1 pont
* A kisebb helyesírási, elgépelési, stilisztikai hibák esetében pedig minden 5. után jár 1-1 pont

### Disclaimer
* Az óráról órára tanulást elváró "témazárós" rendszert 17:2 arányban; az órai kvízeket 19:0 arányban; a lassabb, de biztosabb haladást 18:1 arányban Ti szavaztátok meg az első órán
* A konzultáció időpontjánál 14:4 arányban nyert az az opció, hogy ezek az óra után legyenek. Mivel a 4 érintett jó eséllyel ekkor nem ér rá, itt nem akartam mindenáron az óra utáni megoldást erőltetni. Minden zh előtt szavazunk a konzultációs időpontról, ahol persze az "óra után" is egy opció lesz, egyéb esetleg kedvezőbb időpontok mellett, a legtöbb szavazatot nyerő időpontot fogom választani - amellett, hogy a tananyag is felkerül

### Jegyszerzés

* A fentieknek megfelelően a szorgalmi időszak utolsó napjáig előáll egy összpontszám
* Ezt pótzh-val és azok sikertelensége esetén gyakuv-val lehet majd még javítani (lásd fentebb)
* Ha csak egy-két pont hiányzik, a szorgalmasabbaknak fel fogok ajánlani olyan lehetőséget, hogy (lejárt, be nem adott) szorgalmik közül 4-5 db elkészítésével megadom a jobb jegyet
* A szorgalmi időszak végén írni fogok egy körlevelet, amiben jelzem, a végső pontszámok megszülettek. Arra kérek mindenkit, hogy akár elfogadja ezt a "megajánlott jegyet", akár nem, írjon vissza. Amennyiben elfogadja, a jegy bekerül a neptunba és csakis ez után az illető vizsgára jelentkezhet. Amennyiben nem fogadja el, írja meg, melyik zh-ból vagy zh-kból javítana, és mikor (lehetőleg több időpont-javaslatot is megjelölve). Hamarosan vissza fogok írni a pótzh idejéről és helyéről. A pótzh ha legalábbis a hallgató ezt igényli, a vizsgaidőszak első két hetében kerül megrendezésre. Ha később szeretné, ez nekem is megfelel, de akkor addig nem mehet el vizsgázni sem
* Gyakorlati jegy nélkül ne menjetek vizsgázni
* Ha a saját hibámból valakinek a szorgalmi időszak végére nincs kialakult gyakorlati jegye az alábbi kedvezményeket nyújtom "cserébe":
    * A beadható "kicsi" és "kódolós" szorgalmi feladatok határideje annulálódik, a vizsgaidőszak utolsó napjáig be lehet adni bármit, és bármeddig lehet javítani
    * El lehet menni előre vizsgázni, igazolom a nem beírt gyakorlati jegyet - itt persze, ha a félév legvégéig nem lesz meg a jegy, a vizsgajegy is törlődni fog!
* A jegyszerzésnél az alábbi ponthatárokat veszem figyelembe (félév során elért összes pont):
    * 85-csillagos ég (ez hibajavítások nélkül kb. 135-öt jelent) - jeles
    * 75-84 - jó
    * 65-74 - közepes
    * 55-64 - elégséges
    * 0-54 - elégtelen
