# Algoritmusok

* Számítási lépések jól definiált sorozata illetve egymásba ágyazása a **strukturált programozás** tanult eszközeivel - azaz ebben a félévben ezen alakzatok használatával:
	* Üres utasítás (SKIP)
	* Deklaráció inicializálás nélkül (változó : típusnév - ilyenkor a konstrukort hívja, primitíveknél ha egyértelmű, elhagyjuk)
	* Értékadás (változó := típushelyes kifejezés)
	* Alprogram-hívás (egy már definiált alprogram neve, majd aktuális paraméterlistája - függvény vagy eljárás; ha önmaga más paraméterekkel: rekurzió)
	* Szekvencia (két elemibb program egymás után végrehajtva)
	* Ciklus (elöltesztelős: while; számlálós: for)
	* Elágazás (if; multi-if)
	* return utasítás
* C++-ban kb. ami függvény. Operátorok is ide tartoznak, de pl. a getter-setter metódusok nem (azok technikai, a paradigmából adódó elemek)
* Minden művelet (azaz metódus) függvény is, de vannak típuson kívüli, globális függvények is
* Ezen az órán struktogrammal adjuk meg, de megadható lenne pszeudokóddal, folyamatábrával, konkrét nyelven, bináris alakban is. Maga az algoritmus lényege független a leírásának módjától

## Funkcionális követelmények

* Bemenete és kimenete van
    * Ha metódus, a bemenet impliciten tartalmaz egy példányt az adott típusra
	* A bemenetre **előfeltételt** adunk meg (egy logikai függvény, állítás), azaz csak azokra az inputokra kell helyesen működnie, amik az EF-t teljesítik
	* Az algoritmus "helyessége" pedig azt jelenti, ha egy adott megfelelő bemenetre a futása után a kimeneten előálló értékekre igaz az **utófeltétel**. Azaz az utófeltétel az előfeltételnek megfelelő bemenet és az abból előálló kimenet között fogalmaz meg kapcsolatot, elvárást
    * Az utófeltételt nem fogjuk formálisan leírni, mert ebben a tárgyban nem formális eszközökkel akarunk helyességet bizonyítani, programot szintetizálni
    * Azon logikai állítást, amit az algoritmusnak meg kell tartania, azaz az EF és az UF részei is, **invariánsnak** nevezzük. A **típusinvariáns** pedig egy olyan állítás, ami egy típus összes művelete által helyben hagyandó
    * Egy paraméter lehet akár egyszerre bemenő ÉS kimenő típusú is - elvi szinten ez a mellékhatásos függvény, vagy eljárás; gyakorlati szinten ehhez szoktuk használni a referenciákat

## Fogalmak

* **Algoritmus** (algorithm): minden, ami számítási folyamat
* **Művelet**/**metódus** (operation/method): olyan algoritmus, amit egy adott adatszerkezet osztályában kódolnál le
* **Eljárás** (procedure): aminek a visszatérési típusa void (azaz nem tér vissza semmivel), ellenben valamit manipulál az állapottéren, azaz mellékhatása van
* **Tiszta függvény** (pure function): mellékhatás nélküli függvény, ami az inputból outputot csinál és más semmit
* **Mellékhatásos függvény** (function with side effects): az előző kettő keveréke, csinál is valamit (kiír valamit a konzolra például) és ki is számol valamit, amivel visszatér
* **Struktogram**: egy algoritmus (legyen az akár művelet, függvény, eljárás, bármi) egyik megadási módja

## Nem funkcionális követelmények

* Fontos faktor a helyesség mellett a hatékonyság: időigény, tárigény
	* Abszolút értelemben egyiket se tudjuk megadni, hiszen az algoritmusokat általánosan fogalmazzuk meg (nem ismerjük se a gépet, amin fut, se a körülményeket, a bemenetet, se semmit)
	* **Tárigényt** meg tudjuk adni a bemenet arányában ("n méretű tömb rendezéséhez kell-e vajon egy másik n méretű segédtömb?"). Ezek az input, output, segédváltozók
	* **Időigényt** (lásd még: bonyolultság, komplexitás) pedig a lépésszámmal tudjuk közelíteni
		* Megállapítjuk, mi a domináns művelet (az az, ami sokszor és drágán fut le) és megnézzük n függvényében (ez a bemenet mérete) ez a művelet hányszor fut le
		* Ez egy konkrét függvény lesz, amiket majd nagyságrendjük alapján függvényosztályokba soroljuk

## Paraméterek

* **Formális paraméterlistának** hívjuk a struktogram fejében megadott paraméterlistát. Ott bevezetünk változókat, amelyikekkel majd a paraméterként megadott értékeket vagy referenciákat fogjuk hivatkozni az algoritmus törzsében
    * Alakja: zárójelben "változónév : típus"-párok, vesszővel elválasztva - lehet üres is
* **Aktuális paraméterlista** az adott hívásnál megadott paraméterek összessége. Itt megadhatunk literálokat (konkrét, "konstans" értékek), változókat, de akár összetett kifejezéseket is (pl. függvényhívást), amik be fognak helyettesítődni értékükkel vagy referenciájukkal a hívás helyére
    * A formális és az aktuális paraméterlista i. elemének azonos típusúnak kell lennie (ha az aktuális paraméter egy változó, a névnek természetesen nem kell megegyeznie)
* Ha **referenciát** várunk, akkor azt a formális paraméterlistában jelöljük egy & jellel
	* Az elemi (primitív) típusoknál van csak ez a megkülönböztetés, az összetett adatszerkezetek változóit sose másoljuk, hiszen ezek potenciálisan nagy méretű változók
	* A referencia szerinti paraméterátadásnál nem másoljuk le az átadott adatot a hívás helyén, hanem ténylegesen ugyanazt a példányt használjuk: minden rajta elvégzett módosítás kifelé is hat (mellékhatásos függvény); maga a referencia egy álnév a hívó közeg változójára
	* Az érték szerinti paraméterátadás esetén minden az alprogramban eszközölt módosítás hatástalan az eredeti változóra, ami az aktuális paraméterlistában szerepel, a függvény hívása után ezek a változások elvesznek (kivéve ha nem térünk vissza vele, stb.), úgy vesszük ugyanis, hogy ha pl. egy változót adtunk meg paraméternek, előbb annak az értékét lemásoljuk és magával az értékkel dolgozunk tovább, ami persze független az őt eredetileg tartalmazó változótól
	* Értelemszerűen skalár értékű kifejezés ill. literál nem kerülhet egy aktuális paraméterlistába referencia szerint átadandó paraméter helyébe, ugyanis az ilyen kifejezések mögött nincs változó, amire referenciát lehetne állítani
	* A referenciajelet behelyettesítéskor (aktuális paraméter) nem írjuk ki
	    * Ha referenciát akarnánk átadni, azt a * jellel oldhatjuk meg, de nem fogunk ilyesmit használni (emellett a * jellel fogjuk a pointereket is megadni, ez már egy gyakrabban előforduló eset lesz)
	* Ha egy primitív típusú változót nem akarunk egy algoritmusban módosítani, akkor kifejezetten hibának számít referencia szerint átadni, mivel a programozási nyelvekben ez a paraméterátadási mód általában drágább, mint az érték szerinti
    * A tömb is adatszerkezetnek számít, de paraméterátadásnál úgy vesszük, hogy érték szerint adjuk át az első elem referenciáját
        * Azaz, ha t egy paraméterként átadott tömb, t2 egy másik tömb, akkor a t[0] = 1 hatni fog kifelé, a t = t2 nem, hacsak nem raktuk ki a referenciajelet a t elé
    * Az összes többi adatszerkezet is így működik. Érték szerint adjuk át őket, viszont referenciát adunk át
        * Azaz, ha a Dog egy típus, aminek van egy age adattagja, amit a setAge(N) függvénnyel lehet módosítani, akkor ez a függvény...
        
        ```
        setDogsAge(d : Dog)
            d.setAge(4)
            d : Dog
            d.setAge(5)
        ```
        
        * ... a meghívása után azt a változót, amit a d-be helyettesítettünk módosítani fogja, mégpedig úgy, hogy a kutya kora 4 év lesz
            * Emellett lokálisan készített egy másik változót, amire rá is állította a d-t, de ez a függvényből való kilépéssel automatikusan lebomlott
            * Ha a paraméter referencia szerint lett volna átadva, akkor előbb beállítja a korát 4-re, majd ténylegesen felülírja egy új kutyával, aminek a kora 5 lesz, a többi adattagja pedig a default érték. Az eredeti 4 éves kutya pedig megszűnik
    * Egy paraméternek tehát a függvény hatókörén túl élő új értéket akkor tudunk adni, ha referencia szerint adjuk át, de ha "setter" metódusokkal módosítunk a belső állapotán, az érték szerinti paraméterátadásnál is ér
        * Tömbök esetén az indexelés felfogható ilyesminek (igazából ha t egy tömb, akkor t egyszerre egy tömb és egy pointer a nulladik elemére, t[i] pedig az i. elemére), de magát a t-t újrainicializálni csak akkor lehet, ha referencia szerint adjuk át
        * Ha egy típusban nincsenek setter függvények - azaz, nincsenek olyan függvények, amik a belső állapotát tudják módosítani, azaz inicializálás után már a változó állapota nem változhat, azt a típust **immutable**-nek nevezzük - de ettől még értékadással újra lehet inicializálni
    * Tekintsük az alábbi példát:
    
    ```
        d : Dog
        d.setAge(3)
        set(d)
        print(d.getAge())
    ```
    
    ```
    set(d& : Dog)
        d.setAge(4)
        reallySet(d)
    ```
    
    ```
    reallySet(d : Dog)
        d : Dog
        d.setAge(5)
    ```
    
    * Itt létrehoztunk egy kutyát, a kora kezdetben 3
    * Majd átadtuk referencia szerint egy függvénynek, ami beállította 4-re (itt ha nem referencia szerint adjuk át, akkor is hat kifelé ez a módosítás)
    * Ezután továbbadjuk immár nem referencia szerint, ha ebben a függvényben hívnánk setAge()-et, az ugyanúgy hatna az eredeti kontextusra
    * De itt újrainicializáltuk, ezzel reallySet-ben egy másik kutyánk van, aminek 5-re állítjuk a korát, de setben a d még az eredeti 4 éves kutya
    * Amikor a hívási verem lebomlik akkor a reallySet d-je megszűnik, a főprogramban pedig a 4 éves eredeti d lesz érvényes
    * Tehát hiába adtuk át referencia szerint, ha az új értékadás vagy újrainicializálás nem biztos, hogy élni fog, ha utána továbbdobjuk a változót érték szerint!